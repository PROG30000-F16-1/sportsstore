﻿using Moq;
using Ninject.Modules;
using SportsStore.Domain.Entities;
using SportsStore.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain
{
    public class DomainFactoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IProductCatalog>().To<ProductCatalog>();
            Bind<IProductRepository>().To<DatabaseProductRepository>();
        }
    }
}
