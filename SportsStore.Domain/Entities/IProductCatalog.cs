﻿using System;
using System.Collections.Generic;

namespace SportsStore.Domain.Entities
{
    public interface IProductCatalog
    {
        IEnumerable<Product> ProductList { get; }

        IEnumerable<Product> FilterProducts(Func<Product, bool> matchProduct);

        void SaveProduct(Product product);

        //TODO: add method for deleting a product from the catalog
    }
}