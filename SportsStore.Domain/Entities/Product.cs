﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    public enum ProductCategory : byte
    {
        Soccer = 1,
        WaterSports,
        Chess
    }

    public class Product
    {
        [Required]
        public int ProductID { get; set; }

        [Required(ErrorMessage = "Please provide a name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please provide a description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please provide a category")]
        public ProductCategory Category { get; set; }

        [Required(ErrorMessage = "Please provide a price")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Please enter a positive value for the price.")]
        public decimal Price { get; set; }

        public void Change(Product product)
        {
            this.Name = product.Name;
            this.Description = product.Description;
            this.Price = product.Price;
            this.Category = product.Category;
        }
    }
}
