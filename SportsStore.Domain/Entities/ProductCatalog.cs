﻿using SportsStore.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    class ProductCatalog : IProductCatalog
    {
        private IProductRepository _repository;

        public IEnumerable<Product> ProductList { get; }

        public ProductCatalog(IProductRepository repository)
        {
            _repository = repository;

            this.ProductList = _repository.ProductStore;
        }

        public IEnumerable<Product> FilterProducts(Func<Product, bool> matchProduct)
        {
            foreach (Product currentProduct in ProductList)
            {
                if (matchProduct(currentProduct))
                {
                    yield return currentProduct;
                }
            }
        }

        public void SaveProduct(Product product)
        {
            _repository.SaveProduct(product);
        }

        //TODO: implement the method for deleting a product from the catalog
    }
}
