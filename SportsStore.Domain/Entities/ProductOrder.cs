﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    public class ProductOrder
    {
        public Product Product { get; set; }

        public int Quantity { get; set; }

        public decimal Subtotal
        {
            get { return Product.Price * Quantity; }
        }
    }
}
