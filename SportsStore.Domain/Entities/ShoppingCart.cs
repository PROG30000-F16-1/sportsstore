﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    public class ShoppingCart
    {
        private List<ProductOrder> _productOrderList;

        public IEnumerable<ProductOrder> OrderList
        {
            get { return _productOrderList; }
        }

        public ShoppingCart()
        {
            _productOrderList = new List<ProductOrder>();
        }

        public void AddProduct(Product product, int quantity)
        {
            _productOrderList.Add(new ProductOrder { Product = product, Quantity = quantity });
        }


    }
}
