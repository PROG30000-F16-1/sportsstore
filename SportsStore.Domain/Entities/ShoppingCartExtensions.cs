﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    public static class ShoppingCartExtensions
    {
        public static decimal OrderTotal(this ShoppingCart cart)
        {
            decimal total = 0;
            foreach (ProductOrder order in cart.OrderList)
            {
                total += order.Subtotal;
            }

            return total;
        }
    }
}
