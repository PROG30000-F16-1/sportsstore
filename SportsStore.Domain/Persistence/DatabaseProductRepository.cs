﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsStore.Domain.Entities;

namespace SportsStore.Domain.Persistence
{
    class DatabaseProductRepository : IProductRepository
    {
        private StoreDbContext _dbContext;

        public DatabaseProductRepository()
        {
            _dbContext = new StoreDbContext();
        }

        public IEnumerable<Product> ProductStore
        {
            get
            {
                return _dbContext.Products;
            }
        }

        /// <summary>
        /// Creates new products or changes existing products. A new product
        /// is represented by an ID = 0
        /// </summary>
        /// <param name="product"></param>
        public void SaveProduct(Product product)
        {
            //check if this is a new product
            if (product.ProductID == 0)
            {
                //add the product to the database
                _dbContext.Products.Add(product);
            }
            else
            {
                //change the product IN the database. How?
                //obtain the product FROM the database
                Product prodEntity = _dbContext.Products.Find(product.ProductID);

                //change the product entity itself
                prodEntity.Change(product);
            }

            //apply the changes (add or update) to the database
            _dbContext.SaveChanges();
        }

        //TODO: implement the method for deleting a product from the catalog
    }
}
