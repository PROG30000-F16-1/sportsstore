﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Persistence
{
    public interface IProductRepository
    {
        IEnumerable<Product> ProductStore { get; }

        void SaveProduct(Product product);

        //TODO: add method for deleting a product from the catalog
    }
}
