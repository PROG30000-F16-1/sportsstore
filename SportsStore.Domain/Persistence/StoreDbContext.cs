﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Persistence
{
    class StoreDbContext : DbContext
    {
        public StoreDbContext() : base("SportsStoreDbConnection")
        {
        }

        public DbSet<Product> Products { get; set; }
    }
}
