﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using SportsStore.Domain.Persistence;
using Moq;
using SportsStore.Domain.Entities;
using SportsStore.Domain;
using System.Linq;

namespace SportsStore.UnitTests
{
    /// <summary>
    /// Summary description for ProductFilterUnitTest
    /// </summary>
    [TestClass]
    public class ProductFilterUnitTest
    {
        private IKernel _diKernel = new StandardKernel();

        public ProductFilterUnitTest()
        {
            
        }

        [TestMethod]
        public void FilterByCategory()
        {
            //Step 1: Arrange
            //Create a mock product repository for the IProductRepository interface
            Mock<IProductRepository> testProdRepoMock = new Mock<IProductRepository>();
            testProdRepoMock.Setup(m => m.ProductStore).Returns(
                    new List<Product>
                {
                    new Product {ProductID = 1, Name = "P1", Description=$"P1-{ProductCategory.Soccer}", Price = 1.0m, Category=ProductCategory.Soccer },
                    new Product {ProductID = 2, Name = "P2", Description=$"P2-{ProductCategory.WaterSports}", Price = 10.0m, Category=ProductCategory.WaterSports},
                    new Product {ProductID = 3, Name = "P3", Description=$"P3-{ProductCategory.WaterSports}", Price = 2.0m, Category=ProductCategory.WaterSports }
                });

            _diKernel.Load(new DomainFactoryModule());
            _diKernel.Rebind<IProductRepository>().ToConstant(testProdRepoMock.Object);

            IProductCatalog prodCatalog = _diKernel.Get<IProductCatalog>();

            //Step 2: Act
            //create a filter with one product
            IEnumerable<Product> soccerProdFilter = prodCatalog.FilterProducts(p => p.Category == ProductCategory.Soccer);
            //create a filter with multiple products
            IEnumerable<Product> waterSportsFilter = prodCatalog.FilterProducts(p => p.Category == ProductCategory.WaterSports);
            //create a filter with zero products (empty)
            IEnumerable<Product> chessFilter = prodCatalog.FilterProducts(p => p.Category == ProductCategory.Chess);

            //Step 3: Assert
            Assert.IsTrue(soccerProdFilter.Count() == 1);
            Assert.AreEqual(soccerProdFilter.First().Name, "P1");
            Assert.IsTrue(soccerProdFilter.First().Description.EndsWith(nameof(ProductCategory.Soccer)));

            Assert.IsTrue(waterSportsFilter.Count() == 2);
            Assert.AreEqual(waterSportsFilter.First().Name, "P2");
            Assert.AreEqual(waterSportsFilter.Last().Name, "P3");
            Assert.AreEqual(waterSportsFilter.Where(p => p.Description.EndsWith(nameof(ProductCategory.WaterSports))).Count(), 2);

            Assert.IsTrue(chessFilter.Count() == 0);
        }
    }
}
