﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebApp.Controllers
{
    public class ProductsController : Controller
    {
        private IProductCatalog _catalog;

        public ProductsController(IProductCatalog catalog)
        {
            _catalog = catalog;
        }

        public ActionResult List()
        {
            return View(_catalog.ProductList);
        }

        public ActionResult FilterProducts(string productFilter)
        {
            if (String.IsNullOrEmpty(productFilter))
            {
                return View("List", _catalog.ProductList);
            }
            else
            {
                //ask the product catalog to provide a filtered list of products
                ProductCategory userSelectedCategory = (ProductCategory)Enum.Parse(typeof(ProductCategory), productFilter);

                //use a lambda expression define and use an anoynmous method that 
                //compares the product category with the user selected category
                return View("List", _catalog.FilterProducts(prod => prod.Category == userSelectedCategory));
            }
        }
    }
}