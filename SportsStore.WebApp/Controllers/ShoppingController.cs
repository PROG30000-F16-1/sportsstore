﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebApp.Controllers
{
    public class ShoppingController : Controller
    {
        //using a constant to identify state objects is a best practice as it
        //is typesafe and avoids any possible missspelling of the text identifier
        private const string SHOPPING_CART_SESSION_OBJ = "ShoppingCart";

        private IProductCatalog _catalog;

        public ShoppingController(IProductCatalog catalog)
        {
            _catalog = catalog;
        }

        public ShoppingCart ShoppingCart
        {
            get
            {
                ShoppingCart cart = Session[SHOPPING_CART_SESSION_OBJ] as ShoppingCart;
                if (cart == null)
                {
                    cart = new ShoppingCart();
                    Session[SHOPPING_CART_SESSION_OBJ] = cart;
                }
                return cart;
            }
        }

        public ActionResult Index(string returnUrl)
        {
            //pass the returnUrl receives through the redirect to the view using
            //the ViewBag object as the model for the page is the shopping cart
            ViewBag.ReturnUrl = returnUrl;

            //return the Index view bound to the shopping cart
            return View(this.ShoppingCart);
        }

        public ActionResult AddToCart(int productId, string returnUrl)
        {
            //obtain the product using a LINQ query extension method (FirstOrDefault)
            Product prod = _catalog.ProductList.FirstOrDefault(p => p.ProductID == productId);

            //if product exists, add the product to the shopping cart from the user's session
            if (prod != null)
            {                
                this.ShoppingCart.AddProduct(prod, 1);
            }

            //Redirect to the Index action using an HTTP GET to avoid
            //reposting the form data if user refreshes the page which would
            //result in adding the product to the shopping cart twice
            return RedirectToAction("Index", new { returnUrl });
        }
    }
}