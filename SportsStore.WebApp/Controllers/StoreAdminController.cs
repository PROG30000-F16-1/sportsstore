﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebApp.Controllers
{
    [Authorize]
    public class StoreAdminController : Controller
    {
        private IProductCatalog _catalog;

        public StoreAdminController(IProductCatalog catalog)
        {
            _catalog = catalog;
        }

        //[AllowAnonymous]: Uncomment this to allow users that are not logged
        //in access to see the list of products. Editing and Creating new 
        //products will still be protected and require authorization. You can
        //see an example of AllowAnonymous in the UserAccountsController
        public ActionResult Index()
        {
            return View(_catalog.ProductList);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            if (ModelState.IsValid)
            {
                //add the product to the repository if the model is valid
                _catalog.SaveProduct(product);

                //pass the error message to the view. The view section displaying
                //error messages is standardized and defined in the _DefaultLayout
                TempData["message"] = $"Product {product.Name} added successfully.";

                //if product was successfully added display the list of products
                return RedirectToAction("Index");
            }
            else
            {
                //add an error message
                TempData["message"] = "Could not add product.";

                //if product model is not valid go back to the create form    
                return View(product);
            }


        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            //obtain the product from the catalog
            Product foundProduct = _catalog.ProductList.FirstOrDefault(p => p.ProductID == id);
            if (foundProduct != null)
            {
                //return the view that is bound to the product found
                return View(foundProduct);
            }
            else
            {
                //display and error

                //redirect to the list view
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            //save the product changes in the catalog
            _catalog.SaveProduct(product);

            //redirect to the list of products
            return RedirectToAction("Index");
        }

        //TODO: add action method to display the product details

        //TODO: add action methods to implement the delete operation for the product
    }
}