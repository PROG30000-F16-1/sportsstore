﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SportsStore.WebApp.Infrastructure.Auth;
using SportsStore.WebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebApp.Controllers
{
    [Authorize]
    public class UserAccountsController : Controller
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserViewModel vmUser, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                //if no users exist in the system register this user automatically
                //as a simple mechanism to add the "admin" user of the system
                CheckForFirstUserRegistration(vmUser);

                //Use the store manager to check the user credentials
                SportsStoreUser user = UserManager.Find(vmUser.UserName, vmUser.Password);
                if (user != null)
                {
                    //create an identity to represent this valid user
                    ClaimsIdentity ident = UserManager.CreateIdentity(user,
                                               DefaultAuthenticationTypes.ApplicationCookie);

                    //clear any previous authentication
                    AuthManager.SignOut();
                    
                    //sign the user in using the identity that was created for it
                    AuthManager.SignIn(ident);

                    //redirect to the URL that was requested if one was requested, otherwise
                    //go a default URL, StoreAdmin/Index. ?? is the null-coalescing operator
                    //which provides a short syntax for checking for null
                    return Redirect(returnUrl ?? Url.Action("Index", "StoreAdmin"));
                }
                else
                {
                    //the user authentication failed, display the login view with the
                    //appropriate errors
                    ModelState.AddModelError("", "User name or password are incorrect");
                    ViewBag.ReturnUrl = returnUrl;
                    return View(vmUser);
                }
            }
            else
            {
                return View(vmUser);
            }

        }

        private SportsStoreUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<SportsStoreUserManager>(); }
        }

        private IAuthenticationManager AuthManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        private void CheckForFirstUserRegistration(UserViewModel vmUser)
        {
            //check if this is the first user
            if (this.UserManager.Users.Count() == 0)
            {
                //no users are currently created, create this first one as the administrator
                SportsStoreUser firstUser = new SportsStoreUser { UserName = vmUser.UserName };

                //create an identity for the user by supplying the password that will be 
                //salted and hashed by the Identity framework
                IdentityResult result = this.UserManager.Create(firstUser, vmUser.Password);

                //more error checking could be implemented here to display errors to the user
                Debug.Assert(result.Succeeded);
            }
        }
    }
}