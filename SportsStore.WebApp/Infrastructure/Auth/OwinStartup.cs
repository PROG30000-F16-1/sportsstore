﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Cookies;

[assembly: OwinStartup(typeof(SportsStore.WebApp.Infrastructure.Auth.OwinStartup))]

namespace SportsStore.WebApp.Infrastructure.Auth
{
    public class OwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            //create the database context for Identity
            app.CreatePerOwinContext<SportsStoreIdentDbContext>(SportsStoreIdentDbContext.Create);

            //create the identity user manager
            app.CreatePerOwinContext<SportsStoreUserManager>(SportsStoreUserManager.Create);

            //configure the cookie authentication and the login page
            //configure authentication
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/UserAccounts/Login")
            });
        }
    }
}
