﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsStore.WebApp.Infrastructure.Auth
{
    public class SportsStoreIdentDbContext : IdentityDbContext<SportsStoreUser>
    {
        public SportsStoreIdentDbContext() : base("SportsStoreDbConnection")
        {
            //force the initialization of the database upon start
            Database.Initialize(true);
        }

        /// <summary>
        /// Factory method required by OWIN to instantiate the sports store identity db context
        /// </summary>
        /// <returns></returns>
        public static SportsStoreIdentDbContext Create()
        {
            return new SportsStoreIdentDbContext();
        }
    }
}