﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsStore.WebApp.Infrastructure.Auth
{
    public class SportsStoreUserManager : UserManager<SportsStoreUser>
    {
        public SportsStoreUserManager(IUserStore<SportsStoreUser> store) : base(store)
        {
        }

        public static SportsStoreUserManager Create(
            IdentityFactoryOptions<SportsStoreUserManager> options,
            IOwinContext context)
        {
            SportsStoreIdentDbContext dbContext = context.Get<SportsStoreIdentDbContext>();
            SportsStoreUserManager userManager = new SportsStoreUserManager(new UserStore<SportsStoreUser>(dbContext));
            return userManager;
        }
    }
}